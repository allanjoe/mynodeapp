var mymap = L.map('mapid').setView([19.3,-99.19], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);
var marker = L.marker([19.4,-100.19]).addTo(mymap);
var marker = L.marker([19.3,-99.10]).addTo(mymap);
var marker = L.marker([19.6,-104.45]).addTo(mymap);
var marker = L.marker([19.7,-103.23]).addTo(mymap);
var marker = L.marker([18.2,-102.19]).addTo(mymap);
var marker = L.marker([18.91,-101.19]).addTo(mymap);


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        })
    }
})