var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId){
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

/*var a = new Bicicleta(1, 'rojo', 'urbana', [19.2470747,-99.1544548]);
var b = new Bicicleta(2, 'blanca', 'urbana', [19.2650505,-99.1714139]);
var c = new Bicicleta(3, 'verde', 'urbana', [19.2650505,-99.1714139]);
var d = new Bicicleta(4, 'naranja', 'ciclismo', [19.2650505,-99.1714139]);
var e = new Bicicleta(5, 'amarilla', 'montaña', [19.2650505,-99.1714139]);

Bicicleta.add(a);
Bicicleta.add(b);*/


module.exports = Bicicleta;